# 42 - Security Branch

![purpose](https://img.shields.io/badge/purpose-education-green.svg)
![purpose](https://img.shields.io/badge/42-School-Blue.svg)

Reverse Engineering and Security Projects from 42 School.

- Binary exploitation
- Shellcode
- ELF
- Debugging
- Assembly code 
- Script perl/python/shell 
